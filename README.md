<h1 align="center">
  <p>Run The World Pretest</p>
</h1>


## Intro
This app is built base on `Next.js` with mono repo structure.

## Workflow

Overall, I was inspired by Google Meet and implemented 2 routes:

- `/` (Home)

The user's microphone and webcam permissions will be required.

- `/room/:roomID` (Channel)

Unique room ID, you can share the link to others to join together.

### Features
Everyone can join/leave the channel and mute or unpublish their streaming.


## Development

### install dependencies

```sh
$ yarn install
```


### start the dev server

```sh
$ yarn dev
```

Then open your web browser and check the demo:

http://localhost:3000/

_this app is only tested in latest chrome._
