import React, { useEffect } from 'react'
import styled, { css } from 'styled-components'

import { GetServerSideProps, NextPage } from 'next'

import Link from 'next/link'
import { useSnackbar } from 'notistack'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import Flex from '@/ui/Flex'
import StreamPlayer from '@/ui/StreamPlayer'
import { useAgoraRTC } from '@/ui//hooks'

const Div = styled.div`
  ${() => css``}
`
export type Props = {
  roomID: string
}

const Room: NextPage<Props & React.DOMAttributes<HTMLDivElement>> = ({
  children,
  roomID,
  ...props
}) => {
  const {
    join,
    leave,
    localStream,
    remoteStreamList,
    error,
    isPublished,
    isMuted,
    mute,
    unMute,
    publish,
    unPublish,
  } = useAgoraRTC()
  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    if (error) enqueueSnackbar(error, { variant: 'error' })
  }, [error, enqueueSnackbar])

  useEffect(() => {
    join({ channel: roomID })
  }, [join, roomID])

  return (
    <Div {...props}>
      <AppBar color="primary">
        <Toolbar>
          <Typography className={'font-normal'} variant="h6">
            <Link href="/">
              <a>Home</a>
            </Link>{' '}
            / Room
          </Typography>
        </Toolbar>
      </AppBar>

      <Flex className="justify-between items-center h-screen pt-24 pb-10">
        {localStream ? (
          <>
            <Flex className="w-3/5">
              <StreamPlayer
                isLocal
                isMuted={isMuted}
                isPublished={isPublished}
                agoraStream={localStream}
                onClickMicrophone={isMuted ? unMute : mute}
                onClickCamera={isPublished ? unPublish : publish}
              />
            </Flex>
            <Flex className="w-2/6 flex-col overflow-y-scroll	h-full">
              {remoteStreamList.map((stream) => (
                <div key={stream.getId()} className="mb-5">
                  <StreamPlayer agoraStream={stream} />
                </div>
              ))}
            </Flex>
          </>
        ) : (
          <div className="w-full text-center text-4xl">Loading....</div>
        )}
      </Flex>
    </Div>
  )
}

export default Room

export const getServerSideProps: GetServerSideProps<Props> = async (ctx) => ({
  props: {
    roomID: ctx.params?.roomID as string,
  },
})
