import 'core-js/modules/es.global-this'
import 'tailwindcss/tailwind.css'

import React, { useEffect } from 'react'

import { AppProps } from 'next/app'

import styled, { css } from 'styled-components'

import { MDXProvider } from '@mdx-js/react'

import { SnackbarProvider } from 'notistack'
import { Provider as JotaiProvider } from 'jotai'

import { GlobalStyle } from '@ts-mono/dev-react/components/GlobalStyles'
import { mdxRenders } from '@ts-mono/dev-react/components/mdx-renders'
import { ModalProvider } from '@ts-mono/dev-react/components/Modal'
import GA from '@ts-mono/dev-react/share/GA'

const MaxLayout = styled.div`
  ${() => css`
    max-width: 1000px;
    margin: 0 auto;
    padding: 0 1em;
  `}
`
const ga = new GA('UA-4476856-23', { debug: true })

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  useEffect(() => {
    ga.pageView()
  })
  return (
    <MDXProvider components={mdxRenders}>
      <GlobalStyle />
      <MaxLayout>
        <JotaiProvider>
          <SnackbarProvider
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            autoHideDuration={2500}
            maxSnack={5}
          >
            <ModalProvider>
              <Component {...pageProps} />
            </ModalProvider>
          </SnackbarProvider>
        </JotaiProvider>
      </MaxLayout>
    </MDXProvider>
  )
}

export default App
