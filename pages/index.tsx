import React, { useEffect, useMemo, useRef } from 'react'
import Link from 'next/link'
import { GetServerSideProps } from 'next'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { nanoid } from 'nanoid'

import Flex from '@/ui/Flex'
import RatioBox from '@/ui/RatioBox'
import { useMediaDevice } from '@/ui/hooks'
export type Props = {
  roomID: string
}

const Home: React.FC<Props & React.DOMAttributes<HTMLDivElement>> = ({
  roomID,
}) => {
  const videoRef = useRef<HTMLVideoElement>(null)
  const mediaDevice = useMediaDevice()
  useEffect(() => {
    if (videoRef.current && mediaDevice.stream) {
      videoRef.current.srcObject = mediaDevice.stream
    }
  }, [mediaDevice.stream])

  return (
    <>
      <AppBar color="primary">
        <Toolbar>
          <Typography className={'font-normal'} variant="h6">
            Demo
          </Typography>
        </Toolbar>
      </AppBar>

      <Flex className="justify-between items-center h-screen">
        <div className="w-3/5 flex align-middle">
          <RatioBox
            className="bg-black rounded-md overflow-hidden"
            ratio={[16, 9]}
          >
            {!!mediaDevice.stream && <video ref={videoRef} autoPlay />}
          </RatioBox>
        </div>
        <div className="w-1/3 flex justify-center flex-col">
          {mediaDevice.error ? (
            <p className="text-red-600">
              Please turn on the camera and microphone permissions.
            </p>
          ) : (
            <>
              <div className="font-medium mb-10 w-full text-center text-2xl">
                Your meeting link <br />
                <small className="text-xs text-gray-500">{roomID}</small>
              </div>
              <div
                style={{ background: '#0f796b', width: 120 }}
                className="flex flex-shrink-0 fmin-w-max justify-center cursor-pointer rounded-3xl shadow-md items-center mx-auto"
              >
                <Link href={`/room/${roomID}`}>
                  <a className="inline-block text-white p-2 px-6 hover:no-underline hover:text-white">
                    Join
                  </a>
                </Link>
              </div>
            </>
          )}
        </div>
      </Flex>
    </>
  )
}

export const getServerSideProps: GetServerSideProps<Props> = async () => ({
  props: {
    roomID: nanoid(),
  },
})

export default Home
