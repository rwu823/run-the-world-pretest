declare module 'agora-stream-player' {
  type Props = {
    stream: AgoraRTC.Stream
    label?: string
    fit?: 'contain' | 'cover'
  }

  const StreamPlayer: React.FC<Props>
  export default StreamPlayer
}
