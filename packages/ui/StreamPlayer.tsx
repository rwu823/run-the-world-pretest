import React, { useEffect, useRef, useState } from 'react'
import styled, { css } from 'styled-components'
import VideocamIcon from '@material-ui/icons/VideocamOutlined'
import VideocamOffIcon from '@material-ui/icons/VideocamOffOutlined'

import MicIcon from '@material-ui/icons/MicNoneOutlined'
import MicOffIcon from '@material-ui/icons/MicOffOutlined'

import RatioBox from './RatioBox'
import Flex from './Flex'
export type Props = {
  agoraStream: AgoraRTC.Stream & { stream: MediaStream }
  isLocal?: boolean
  isMuted?: boolean
  isPublished?: boolean
  onClickMicrophone?: () => void
  onClickCamera?: () => void
}

const Control = styled.div<{ isDisabled?: boolean }>`
  ${(p) => css`
    border: 1px solid #fff;
    border-radius: 50%;
    padding: 0.7rem;
    cursor: pointer;
    margin: 0 0.7rem;
    svg {
      width: 1.8rem;
      height: 1.8rem;

      fill: #fff;
    }

    :hover {
      background: rgba(255, 255, 255, 0.4);
    }

    ${p.isDisabled &&
    css`
      background: #c94031;

      :hover {
        background: #a73a2e;
      }
    `}
  `}
`

const MuteControl = styled(Control)``
const CameraControl = styled(Control)``

const StreamPlayer: React.FC<Props & React.DOMAttributes<HTMLVideoElement>> = ({
  children,
  agoraStream,
  isLocal,
  isMuted,
  isPublished,
  onClickCamera,
  onClickMicrophone,
  ...props
}) => {
  const videoRef = useRef<HTMLVideoElement>(null)

  useEffect(() => {
    if (videoRef.current) videoRef.current.srcObject = agoraStream.stream
  }, [agoraStream])

  return (
    <RatioBox
      ratio={[16, 9]}
      className="rounded-md overflow-hidden relative bg-black"
    >
      <video
        autoPlay
        className="object-cover w-full h-full"
        ref={videoRef}
        {...props}
      />

      <div className="absolute w-full left-0 bottom-0">
        {isLocal && (
          <Flex className="justify-center py-5">
            <MuteControl isDisabled={isMuted} onClick={onClickMicrophone}>
              {isMuted ? <MicOffIcon /> : <MicIcon />}
            </MuteControl>
            <CameraControl isDisabled={!isPublished} onClick={onClickCamera}>
              {isPublished ? <VideocamIcon /> : <VideocamOffIcon />}
            </CameraControl>
          </Flex>
        )}

        <strong className="h-10 text-center bg-black bg-opacity-50 text-white  flex items-center justify-center">
          ID: {agoraStream.getId()}
        </strong>
      </div>
    </RatioBox>
  )
}

export default StreamPlayer
