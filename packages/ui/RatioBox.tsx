import React from 'react'
import styled, { css } from 'styled-components'

const Div = styled.div<Props>`
  ${(p) => css`
    position: relative;
    width: 100%;

    &:before {
      content: '';
      display: block;
      padding-top: ${(p.ratio[1] / p.ratio[0]) * 100}%;
    }
    > :first-child {
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
    }
  `}
`
export type Props = {
  ratio: [number, number]
  className?: string
}

export const RatioBox: React.FC<
  Props & React.DOMAttributes<HTMLDivElement>
> = ({ children, ratio, ...props }) => (
  <Div ratio={ratio} {...props}>
    <div>{children}</div>
  </Div>
)

export default RatioBox
