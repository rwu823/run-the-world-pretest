import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { nanoid } from 'nanoid'
import { getAgoraRTC } from '@/shared/services/AgoraRTC'
import { APP_ID, CHANNEL } from '@/shared/consts'

import { Props as StreamPlayerProps } from '@/ui/StreamPlayer'

type StreamEvent = (event: { stream: StreamPlayerProps['agoraStream'] }) => void

type JoinOptions = Partial<{
  mode: 'rtc' | 'live'
  codec: 'h264' | 'vp8'
  channel: string
}>

interface EnhanceAgoraClient extends AgoraRTC.Client {
  gatewayClient: {
    removeEventListener: (event: string, onEvent: StreamEvent) => void
  }
}

export const useAgoraRTC = () => {
  const [localStream, setLocalStream] = useState<
    StreamPlayerProps['agoraStream']
  >()
  const [error, setError] = useState(null)
  const [isPublished, setPublished] = useState(false)
  const [isMuted, setMute] = useState(false)
  const [remoteStreamList, setRemoteStreamList] = useState<
    StreamPlayerProps['agoraStream'][]
  >([])

  const [uid, setUID] = useState<string>()

  const [client, setClient] = useState<EnhanceAgoraClient>()

  const addLocal = useCallback<StreamEvent>((e) => {
    const { stream } = e

    setLocalStream(stream)
  }, [])

  const onSubscribe = useCallback<StreamEvent>(
    (e) => {
      if (client && e.stream) {
        client.subscribe(e.stream)
      }
    },
    [client],
  )

  const addRemote = useCallback<StreamEvent>((e) => {
    const { stream } = e

    setRemoteStreamList((streamList) => [
      ...streamList.filter((s) => s.getId() !== stream.getId()),
      stream,
    ])
  }, [])

  const removeRemote = useCallback<StreamEvent>((e) => {
    const { stream } = e
    if (stream) {
      setRemoteStreamList((streamList) =>
        streamList.filter((s) => s.getId() !== stream.getId()),
      )
    }
  }, [])

  const leave = useCallback(async () => {
    if (client && localStream) {
      try {
        localStream.close()

        await client.unpublish(localStream)

        // leave the channel
        await client.leave()

        setLocalStream(undefined)
        setPublished(false)
      } catch (err) {
        setError(err)
      }
    }
  }, [localStream, client])

  const unPublish = useCallback(async () => {
    if (localStream && client) {
      try {
        // await client.unpublish(localStream)
        setPublished(!localStream.muteVideo())
      } catch (err) {
        setError(err)
      }
    }
  }, [localStream, client])

  const publish = useCallback(async () => {
    try {
      if (localStream && client) {
        // await client.publish(localStream)

        setPublished(localStream.unmuteVideo())
      }
    } catch (err) {
      setError(err)
    }
  }, [localStream, client])

  const mute = useCallback(() => {
    if (localStream) {
      setMute(localStream.muteAudio())
    }
  }, [localStream])

  const unMute = useCallback(() => {
    if (localStream) {
      setMute(!localStream.unmuteAudio())
    }
  }, [localStream])

  const join = useCallback(
    async ({
      mode = 'rtc',
      codec = 'h264',
      channel = CHANNEL,
    }: JoinOptions = {}) => {
      const AgoraRTC = await getAgoraRTC()
      try {
        const newClient = AgoraRTC.createClient({
          mode,
          codec,
        })

        const streamID = nanoid(10)

        // @ts-expect-error
        setClient(newClient)
        setUID(streamID)

        await newClient.init(APP_ID)
        await newClient.join(null, channel, streamID)

        const stream = AgoraRTC.createStream({
          streamID,
          video: true,
          audio: true,
          screen: false,
        })

        await stream.init()
        await newClient.publish(stream)
        setPublished(true)
      } catch (err) {
        setError(err)
      }
    },
    [],
  )

  useEffect(() => {
    const onWindowClose = () => {
      leave()
    }
    window.addEventListener('beforeunload', onWindowClose)

    if (client) {
      client.on('stream-published', addLocal)
      client.on('stream-added', onSubscribe)
      client.on('stream-subscribed', addRemote)
      client.on('stream-removed', removeRemote)
      // client.on('peer-leave', removeRemote)
    }

    return () => {
      if (client) {
        client.off('stream-published', addLocal)
        client.off('stream-added', addRemote)
        client.off('stream-subscribed', onSubscribe)
        client.off('stream-removed', removeRemote)
        // client.off('peer-leave', removeRemote)

        leave()
      }

      window.removeEventListener('beforeunload', onWindowClose)
    }
  }, [leave, addLocal, addRemote, onSubscribe, removeRemote, client])

  return {
    mute,
    unMute,
    isMuted,
    error,
    isPublished,
    uid,
    join,
    leave,
    publish,
    unPublish,
    localStream,
    remoteStreamList,
  }
}
