/* eslint-disable compat/compat */
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
export const useMediaDevice = ({
  audio = true,
  video = true,
  peerIdentity,
}: MediaStreamConstraints = {}) => {
  const [stream, setStream] = useState<MediaStream>()
  const [error, setError] = useState<string>()

  useEffect(() => {
    navigator.mediaDevices
      .getUserMedia({
        audio,
        video,
        peerIdentity,
      })
      .then((s) => {
        setStream(s)
      })
      .catch((err) => {
        setError(err.message)
      })
  }, [audio, peerIdentity, video])

  return { stream, error }
}
