export const getAgoraRTC = () =>
  Promise.all([
    import('agora-rtc-sdk'),
    import('agoran-awe'),
  ]).then(([{ default: AgoraSDK }, { default: enhanceAgora }]) =>
    enhanceAgora(AgoraSDK),
  )
